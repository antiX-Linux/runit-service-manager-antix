��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  �  �     g     o  '   �     �     �  	   �     �     �     �     �  	   �            	   (     2     :     B     J  4   R     �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:03+0100
Last-Translator: Wallon
Language-Team: Spanish (https://app.transifex.com/anticapitalista/teams/10162/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.2.2
 Agregar Agregar servicio no utilizado Todos los servicios ya están cargados. Deshabilitar Inactivo Habilitar Registro (Log) No Recargar Remover Reiniciar Gestor de servicios de Runit Servicio Servicios Iniciar Inicio: Estado: Detener Este es un servicio VITAL (no se puede deshabilitar) Activo Sí 