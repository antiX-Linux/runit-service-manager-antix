��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  �  �  	   v  !   �  )   �  	   �  
   �     �     �     �  
   �       	   	          -  	   6     @     H     ]     e  1   k     �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:04+0100
Last-Translator: Wallon
Language-Team: Portuguese (https://app.transifex.com/anticapitalista/teams/10162/pt/)
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.2.2
 Adicionar Adicionar serviço não utilizado Todos os serviços já estão carregados. Desativar Desativado Activar Registo Não Recarregar Remover Reiniciar Gestor de Serviços Runit Serviço Serviços Iniciar Início automático: Estado: Parar Este serviço é VITAL (não pode ser desativado) Ativado Sim 