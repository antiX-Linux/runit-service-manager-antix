��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  �  �     g     p  #   �  
   �     �     �     �     �     �     �     �     �          
                    &  ;   +     g     j                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:04+0100
Last-Translator: Wallon
Language-Team: Italian (https://app.transifex.com/anticapitalista/teams/10162/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.2.2
 Aggiungi Aggiungi servizio inutilizzato Tutti i servizi sono già caricati. Disabilita Giù Abilita Log No Ricarica Rimuovi Riavvia Gestore servizi Runit Servizio Servizi Avvia Avvio: Stato: Stop Questo è un servizio VITALE (non può essere disabilitato) Su Sì 