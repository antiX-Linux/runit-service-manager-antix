��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  �  �     r     z  '   �     �     �     �     �     �  	   �  	   �     �          &     .  	   7     A     Z     b  ?   k     �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:03+0100
Last-Translator: Wallon
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Generator: Poedit 3.2.2
 Ajouter Ajouter un service inutilisé Tous les services sont déjà chargés. Désactiver Désactivé Activer Log Non Recharger Supprimer Redémarrer Gestionnaire de service Runit Service Services Démarrer Démarrage automatique : État : Arrêter Il s’agit d’un service VITAL (il ne peut être désactivé) Activé Oui 