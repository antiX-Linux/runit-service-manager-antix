��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �    �     �  :   �  .   %     T     g     p     �     �     �     �     �  +   �             
        %     3     A  G   J  
   �     �                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:05+0100
Last-Translator: Wallon
Language-Team: Russian (Russia) (https://app.transifex.com/anticapitalista/teams/10162/ru_RU/)
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 3.2.2
 Добавить Добавить неиспользуемую службу Все службы уже загружены. Отключить Вниз Включить Журнал Нет Перезагрузить Удалить Перезапуск Управление службами Runit Служба Службы Старт Запуск: Статус: Стоп Это ВАЖНАЯ служба (ее нельзя отключить) Вверх Да 