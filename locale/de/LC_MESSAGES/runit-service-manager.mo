��          �      |      �     �     �           )     1     6     =     A     D     K     R     Z     p     x     �     �     �     �  /   �     �     �  j  �     ?  "   K  "   n     �     �  
   �  	   �     �  	   �  	   �     �     �     �     �          
            @   (     i     o                                                                                   	       
       Add Add unused service All services are already loaded. Disable Down Enable Log No Reload Remove Restart Runit Service Manager Service Services Start Startup: Status: Stop This is a VITAL service (it cannot be disabled) Up Yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-27 16:02+0100
Last-Translator: Wallon
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Hinzufügen Nicht genutzten Dienst hinzufügen Alle Dienste sind bereits geladen. Deaktivieren inaktiv Aktivieren Protokoll Nein Neu laden Entfernen Neustart runit-Diensteverwaltung Dienst Dienste Start Systemstart: Status: Anhalten Dieser Dienst ist essentiell (er kann nicht deaktiviert werden). aktiv Ja 